﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSolution
{
    public class DayCareStreamInterpreter
    {
        private Dictionary<string, HashSet<string>> results=new Dictionary<string,HashSet<string>>();
        public Dictionary<string, HashSet<string>> Results { get; private set; }

        public string GetFavoriteName(Stream input) 
        {
            results.Clear();
            var reader = new StreamReader(input, Encoding.UTF8);
            while(!reader.EndOfStream)
            { 
            
                var line = reader.ReadLine();
            
                var names = line.Split(new string[0] , StringSplitOptions.RemoveEmptyEntries);
                //because empty line and/or only voter name
                if(names.Count()>1){
                var voterName = names[0];
                if(!results.ContainsKey(voterName))
                {
                    results.Add(voterName, new HashSet<string>());
                }
                var previousVotes = results[voterName];
                    foreach(var name in names.Where(n=>n!=voterName))
                    {
                        previousVotes.Add(name);
                    }
                }
            }
            results = results.Where(n=>n.Value.Count>0).ToDictionary(n=>n.Key,n=>n.Value);
            if(results.Any()){
            //var best = (results.FirstOrDefault(m => m.Value.Count == results.Select(n => n.Value.Count).Max()));
                return results.SelectMany(n => n.Value).GroupBy(n => n)
                    .OrderByDescending(n => n.Count()).Select(n => n.Key).FirstOrDefault();//best.Key;
            }
            else { return ""; }
        }
    }
}
