﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestSolution;
using System.IO;

namespace UnitTest
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestCase1()
        {
            var expected = "";
            var input = "kisa kisa";
            string retVal = GetResults(input);
            if (retVal != expected)
            {
                throw new Exception("Test case 1(self-voting) failed");
            }
        }

        [TestMethod]
        public void TestCase2()
        {
            var expected = "kisb";
            var input = "kisa kisa\n kisa kisb\n kisc kisb";
            string retVal = GetResults(input);
            if (retVal != expected)
            {
                throw new Exception("Test case 2(simple voting) failed");
            }
        }

        [TestMethod]
        public void TestCase3()
        {
            var expected = "kisc";
            var input = "kisa kisc\n kisa kisb\n kisa kisb\n kisb kisc";
            string retVal = GetResults(input);
            if (retVal != expected)
            {
                throw new Exception("Test case 3(multiple voting) failed");
            }
        }
        private static string GetResults(string input)
        {
            var ObjectToTestOn = new DayCareStreamInterpreter();
            string retVal;
            MemoryStream stream = null; StreamWriter writer = null;
            try
            {
                stream = new MemoryStream();

                writer = new StreamWriter(stream, System.Text.Encoding.UTF8);
                writer.Write(input);
                writer.Flush();//otherwise you are risking empty stream
                stream.Seek(0, SeekOrigin.Begin);
                retVal = ObjectToTestOn.GetFavoriteName(stream);

            }

            finally
            {
                if (writer != null) { writer.Close(); }
                // if (stream != null) { stream.Dispose(); }
            }
            return retVal;
        }
    }
}
